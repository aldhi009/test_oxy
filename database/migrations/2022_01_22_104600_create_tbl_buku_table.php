<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_buku', function (Blueprint $table) {
            $table->id();
            $table->string('kode_buku', 10);
            $table->unsignedBigInteger('id_kategori');
            $table->foreign('id_kategori')->references('id')->on('tbl_kategori');
            $table->unsignedBigInteger('id_rak');
            $table->foreign('id_rak')->references('id')->on('tbl_rak');
            $table->bigInteger('isbn');
            $table->string('judul');
            $table->string('penerbit');
            $table->integer('tahun');
            $table->bigInteger('jumlah');
            $table->date('tgl_masuk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_buku');
    }
}
