<?php

namespace Database\Seeders;

use App\Models\Kategori;
use Illuminate\Database\Seeder;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'nama_kategori' => 'Komputer',

            ],
            [
                'nama_kategori' => 'IPA',

            ],
            [
                'nama_kategori' => 'IPS',

            ],
        ];

        foreach($data as $d){
            Kategori::create($d);
        }
    }
}
