<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AkunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'admin',
                'email' => 'admin@test.com',
                'password' => bcrypt('123456789'),
                'role' => 'ADMIN',
                'status' => '1',
            ],
            [
                'name' => 'siswa1',
                'email' => 'siswa1@test.com',
                'password' => bcrypt('123456789'),
                'role' => 'SISWA',
                'status' => '1',
            ],
            [
                'name' => 'siswa2',
                'email' => 'siswa2@test.com',
                'password' => bcrypt('123456789'),
                'role' => 'SISWA',
                'status' => '1',
            ],
        ];

        foreach($data as $d){
            User::create($d);
        }
    }
}
