<?php

namespace Database\Seeders;

use App\Models\Rak;
use Illuminate\Database\Seeder;

class RakSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'nama_rak' => 'Rak 1',

            ],
            [
                'nama_rak' => 'Rak 2',

            ],
            [
                'nama_rak' => 'Rak 3',

            ],
        ];

        foreach($data as $d){
            Rak::create($d);
        }
    }
}
