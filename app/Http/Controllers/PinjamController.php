<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use App\Models\Pinjam;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PinjamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ( $request->user()->role != 'ADMIN') {
            return redirect('/');
        }
        $pinjam = Pinjam::all();
        $buku = Buku::all();
        $user = User::where('role', 'SISWA')->get();

        return view('pinjam.index',compact('pinjam', 'buku', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request,[
            'id_buku'     => 'required',
            'id_user'     => 'required',
        ]);

        $simpan['id_buku']      =  $request['id_buku'];
        $simpan['id_user']      =  $request['id_user'];
        $simpan['status']       =  'Dipinjam';
        $simpan['tgl_pinjam']   =  Carbon::now()->format('Y-m_d');

        Pinjam::create($simpan);

            $update = Buku::where('id', $request['id_buku'])->first();
            $update['jumlah'] = ($update['jumlah']-1);
            $update->save();

        return back()->with('hijau', 'Buku '.$update['judul'].' telah dipinjam.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pinjam  $pinjam
     * @return \Illuminate\Http\Response
     */
    public function show(Pinjam $pinjam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pinjam  $pinjam
     * @return \Illuminate\Http\Response
     */
    public function edit(Pinjam $pinjam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pinjam  $pinjam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pinjam $pinjam)
    {

        $request['tgl_kembali'] = Carbon::now()->format('Y-m-d');
        $request['status'] = 'Dikembalikan';

        $pinjam->update($request->all());

            $update = Buku::where('id', $pinjam->id_buku)->first();
            $update['jumlah'] = ($update['jumlah']+1);
            $update->save();

        return back()->with('hijau', 'Peminjaman buku '.$update['judul'].' telah dikembalikan.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pinjam  $pinjam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pinjam $pinjam)
    {
        $pinjam = Pinjam::find($pinjam->id);
        $pinjam->delete();

            $update = Buku::where('id', $pinjam->id_buku)->first();
            $update['jumlah'] = ($update['jumlah']+1);
            $update->save();

        return back()->with('merah', 'Peminjaman buku telah dihapus.');
    }
}
