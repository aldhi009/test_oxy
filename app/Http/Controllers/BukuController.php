<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use App\Models\Kategori;
use App\Models\Rak;
use Illuminate\Http\Request;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ( $request->user()->role != 'ADMIN') {
            return redirect('/');
        }

        $buku = Buku::all();
        $kategori = Kategori::all();
        $rak = Rak::all();

        return view('buku.index',compact('buku', 'kategori', 'rak'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kode_buku'   => 'required|max:10',
            'id_kategori' => 'required',
            'id_rak'      => 'required',
            'isbn'        => 'required|numeric',
            'judul'       => 'required',
            'penerbit'    => 'required',
            'tahun'       => 'required',
            'jumlah'      => 'required',
            'tgl_masuk'   => 'required|date',
        ]);

        $simpan['kode_buku']    =  $request['kode_buku'];
        $simpan['id_kategori']  =  $request['id_kategori'];
        $simpan['id_rak']       =  $request['id_rak'];
        $simpan['isbn']         =  $request['isbn'];
        $simpan['judul']        =  $request['judul'];
        $simpan['penerbit']     =  $request['penerbit'];
        $simpan['tahun']        =  $request['tahun'];
        $simpan['jumlah']       =  $request['jumlah'];
        $simpan['tgl_masuk']    =  $request['tgl_masuk'];

        Buku::create($simpan);

        return back()->with('hijau', 'Data Buku telah ditambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function show(Buku $buku)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function edit(Buku $buku)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Buku $buku)
    {
        $this->validate($request,[
            'kode_buku'   => 'required|max:10',
            'id_kategori' => 'required',
            'id_rak'      => 'required',
            'isbn'        => 'required|numeric',
            'judul'       => 'required',
            'penerbit'    => 'required',
            'tahun'       => 'required',
            'jumlah'      => 'required',
            'tgl_masuk'   => 'required|date',
        ]);

        $buku->update($request->all());

        return back()->with('hijau', 'Data Buku '.$buku->judul.' telah diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Buku  $buku
     * @return \Illuminate\Http\Response
     */
    public function destroy(Buku $buku)
    {
        Buku::find($buku->id)->delete();

        return back()->with('merah', 'Buku '.$buku->judul.' Telah dihapus.');
    }
}
