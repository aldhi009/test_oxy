<?php

namespace App\Http\Controllers;

use App\Models\Pinjam;
use Illuminate\Http\Request;

class AplikasiController extends Controller
{
    public function dashboard(Request $request)
    {
        $siswa = Pinjam::where('id_user', $request->user()->id)->get();

        return view('dashboard', compact('siswa'));
    }

}
