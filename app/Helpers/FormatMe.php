<?php
function format_uang($angka){
     $hasil=number_format($angka,0,',','.');
     return $hasil;
}

function format_tanggaljam($tanggal)
{
     $hasil = date('d M Y - H:i:s', strtotime($tanggal));
     return $hasil;
}

function format_tanggal($tanggal)
{
     $hasil = Carbon\Carbon::parse($tanggal)->translatedFormat('l, d F Y');
     return $hasil;
}

function tanggal($tanggal)
{
     $hasil = Carbon\Carbon::parse($tanggal)->translatedFormat('d F Y');
     return $hasil;
}

function format_jam($jam)
{
     $hasil = date('H:i:s', strtotime($jam));
     return $hasil;
}

function durasi($awal, $akhir)
{
     $hasil  = $awal->diffInDays($akhir);
     return $hasil;
}