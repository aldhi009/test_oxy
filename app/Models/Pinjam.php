<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pinjam extends Model
{
    use HasFactory;

    protected $table = 'tbl_pinjam';

    protected $guarded = ['id'];

    public function getuser()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function getbuku()
    {
        return $this->belongsTo(Buku::class, 'id_buku');
    }
}
