<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    use HasFactory;

    protected $table = 'tbl_buku';

    protected $guarded = ['id'];

    public function getkategori()
    {
        return $this->belongsTo(Kategori::class, 'id_kategori');
    }

    public function getrak()
    {
        return $this->belongsTo(Rak::class, 'id_rak');
    }
}
