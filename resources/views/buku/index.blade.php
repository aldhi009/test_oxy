@extends('template.template')

{{-- SUBMENU --}}
@section('sub-menu')

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item active" aria-current="page"><span>Buku</span></li>
</ol>

@endsection

@section('buku')

data-active="true"

@endsection

{{-- CSS --}}
@section('css')

<style>
    .table>tbody>tr>td {
        color: #000000;
    }
</style>

@endsection

@section('content')

<!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing">

            <div class="col-xl-12 col-lg-12 col-sm-12 mb-3">

                {{-- menampilkan error validasi --}}
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if ($message = Session::get('hijau'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif

                @if ($message = Session::get('merah'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif


                <button type="button" class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#tambah">
                    Tambah Buku
                </button>


                <div class="card shadow mb-3">
                    <div class="card-body">
                        <div class="table-responsive mb-4 mt-4">
                            <table id="datatables" class="table nowrap table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode</th>
                                        <th>Kategori</th>
                                        <th>Rak</th>
                                        <th>ISBN</th>
                                        <th>Judul (thn)</th>
                                        <th>Penerbit</th>
                                        <th>Jumlah</th>
                                        <th>Tgl Masuk</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                @php
                                $no = 1;
                                @endphp
                                <tbody>
                                    @foreach ($buku as $data)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $data['kode_buku'] }}</td>
                                        <td>{{ $data->getkategori['nama_kategori'] }}</td>
                                        <td>{{ $data->getrak['nama_rak'] }}</td>
                                        <td>{{ $data['isbn'] }}</td>
                                        <td>{{ $data['judul'] }} ({{ $data['tahun'] }})</td>
                                        <td>{{ $data['penerbit'] }}</td>
                                        <td>{{ $data['jumlah'] }}</td>
                                        <td>{{ tanggal($data['tgl_masuk']) }}</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                                                data-target="#edit{{ $data['id'] }}">
                                                Edit
                                            </button>

                                            @if(\App\Models\Pinjam::where('id_buku', $data['id'])->count() >= 1)
                                            <button type="button" class="btn btn-danger btn-sm" disabled>
                                                Hapus
                                            </button>
                                            @else
                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                                data-target="#hapus{{ $data['id'] }}">
                                                Hapus
                                            </button>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <div class="footer-wrapper">
        <div class="footer-section f-section-1">
            <p class="">Copyright © 2021 <a target="_blank" href="https://designreset.com">ALDHI</a>, Test OXY.</p>
        </div>
        <div class="footer-section f-section-2">
            <p class="">Coded with <a href="http://instagram.com/aldhi009" target="_blank" rel="noopener noreferrer"
                    class="text-primary">Aldhi</a></p>
        </div>
    </div>
</div>
<!--  END CONTENT AREA  -->


{{-- MODAL TAMBAH BUKU --}}
<div class="modal fade" id="tambah">
    <div class="modal-dialog modal-lg">
        <form action="{{ route('buku.store') }}" method="post">
            @csrf
            @method('POST')
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah buku</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        x
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kode_buku">Kode Buku</label>
                                <input type="text" name="kode_buku" id="kode_buku" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="judul">Judul</label>
                                <input type="text" name="judul" id="judul" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="id_kategori">Kategori</label>
                                <select name="id_kategori" class="form-control" required>
                                    <option value="">Pilih</option>
                                    @foreach ($kategori as $d)
                                    <option value="{{ $d['id'] }}">{{ $d['nama_kategori'] }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="id_rak">Rak</label>
                                <select name="id_rak" class="form-control" required>
                                    <option value="">Pilih</option>
                                    @foreach ($rak as $d)
                                    <option value="{{ $d['id'] }}">{{ $d['nama_rak'] }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="isbn">ISBN</label>
                                <input type="number" name="isbn" id="isbn" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="penerbit">Penerbit</label>
                                <input type="text" name="penerbit" id="penerbit" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="tahun">Tahun</label>
                                <select name="tahun" class="form-control" required>
                                    <option value="">Pilih</option>
                                    @for ($i= 2000; $i<=2022; $i++) <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="jumlah">Jumlah</label>
                                <input type="number" name="jumlah" id="jumlah" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="tgl_masuk">Tanggal Masuk</label>
                                <input type="date" name="tgl_masuk" id="tgl_masuk" class="form-control" required>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Batal</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </div>
        </form>
    </div>

</div>


{{-- EDIT BUKU  --}}
@foreach($buku as $databuku)

<div class="modal fade" id="edit{{ $databuku['id'] }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form action="{{ route('buku.update', $databuku['id']) }}" method="post">
            @csrf
            @method('PUT')
            <div class="modal-content">


                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit buku {{ $databuku['judul'] }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        x
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kode_buku">Kode Buku</label>
                                <input type="text" name="kode_buku" id="kode_buku" class="form-control"
                                    value="{{ $databuku['kode_buku'] }}" required>
                            </div>


                            <div class="form-group">
                                <label for="judul">Judul</label>
                                <input type="text" name="judul" id="judul" class="form-control"
                                    value="{{ $databuku['judul'] }}" required>
                            </div>

                            <div class="form-group">
                                <label for="id_kategori">Kategori</label>
                                <select name="id_kategori" class="form-control" required>
                                    <option value="">Pilih</option>
                                    @foreach ($kategori as $d)
                                    <option value="{{ $d['id'] }}"
                                        {{ ( $d['id'] == $data['id_kategori'] ) ? 'selected' : '' }}>
                                        {{ $d['nama_kategori'] }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="id_rak">Rak</label>
                                <select name="id_rak" class="form-control" required>
                                    <option value="">Pilih</option>
                                    @foreach ($rak as $d)
                                    <option value="{{ $d['id'] }}"
                                        {{ ( $d['id'] == $data['id_rak'] ) ? 'selected' : '' }}>
                                        {{ $d['nama_rak'] }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="isbn">ISBN</label>
                                <input type="number" name="isbn" id="isbn" class="form-control"
                                    value="{{ $databuku['isbn'] }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="penerbit">Penerbit</label>
                                <input type="text" name="penerbit" id="penerbit" class="form-control"
                                    value="{{ $databuku['penerbit'] }}" required>
                            </div>

                            <div class="form-group">
                                <label for="tahun">Tahun</label>
                                <select name="tahun" class="form-control" required>
                                    <option value="">Pilih</option>
                                    @for ($i= 2000; $i<=2022; $i++) <option value="{{ $i }}"
                                        {{ ( $databuku['tahun'] == $i ) ? 'selected' : '' }}>{{ $i }}
                                        </option>
                                        @endfor
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="jumlah">Jumlah</label>
                                <input type="number" name="jumlah" id="jumlah" class="form-control"
                                    value="{{ $databuku['jumlah'] }}" required>
                            </div>

                            <div class="form-group">
                                <label for="tgl_masuk">Tanggal Masuk</label>
                                <input type="date" name="tgl_masuk" id="tgl_masuk" class="form-control"
                                    value="{{ $databuku['tgl_masuk'] }}" required>
                            </div>
                        </div>
                    </div>





                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Batal</button>
                    <button type="submit" class="btn btn-warning">Ubah</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endforeach

{{-- MODAL HAPUS KARYAWAN --}}
@foreach($buku as $databuku)
<div class="modal fade" id="hapus{{ $databuku['id'] }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h5 class="modal-title">Hapus buku {{ $databuku['judul'] }}</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <h6>Data yang dihapus tidak akan bisa dikembalikan.</h6>
            </div>
            <form action="{{ route('buku.destroy', $databuku['id']) }}" method="post">
                @csrf
                @method('DELETE')
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Batal</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach


@endsection


@section('js')

<script>
    $('#datatables').DataTable({
        dom: '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-10"i><"col-md-2 mb-3"p>>> >',
        buttons: {
            buttons: [{
                    extend: 'copy',
                    className: 'btn',
                    footer: true
                },
                {
                    extend: 'csv',
                    className: 'btn',
                    footer: true
                },
                {
                    extend: 'excel',
                    className: 'btn',
                    footer: true
                },
                {
                    extend: 'print',
                    className: 'btn',
                    footer: true
                }
            ]
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Results :  _MENU_",
        },
        "stripeClasses": [],
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        "pageLength": 10,
        "pagingType": "simple",

    });
</script>

@endsection
