@extends('template.template')

{{-- SUBMENU --}}
@section('sub-menu')

<ol class="breadcrumb">
    <li class="breadcrumb-item active" aria-current="page"><span>Dashboard</span></li>
</ol>

@endsection

@section('dashboard')

data-active="true"

@endsection

{{-- CSS --}}
@section('css')

<style>
    .table>tbody>tr>td {
        color: #000000;
    }
</style>

@endsection

@section('content')

<!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing">

            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    {{ format_tanggal(Carbon\Carbon::now()) }}
                    <h4>Welcome {{ Auth::user()->name }}.</h4>
                </div>
            </div>

            @if (Auth::user()->role == 'SISWA')

            {{-- @if (!empty($siswa[0])) --}}

            <div class="col-md-12">
                <h5>PEMINJAMAN</h5>
                <div class="card shadow mb-3">
                    <div class="card-body">
                        <div class="table-responsive mb-4 mt-4">
                            <table id="datatables" class="table nowrap table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Buku</th>
                                        <th>status</th>
                                        <th>Tanggal pinjam</th>
                                        <th>Tanggal kembali</th>
                                    </tr>
                                </thead>
                                @php
                                $no = 1;
                                @endphp
                                <tbody>
                                    @foreach ($siswa as $data)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $data->getbuku['kode_buku'] }} - {{ $data->getbuku['judul'] }}</td>
                                        <td>{{ $data['status'] }}</td>
                                        <td>{{ tanggal($data['tgl_pinjam']) }}</td>
                                        <td>
                                            @if (!empty($data['tgl_kembali']))
                                                {{ tanggal($data['tgl_kembali']) }}
                                            @else
                                                -
                                            @endif
                                            </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            {{-- @endif --}}

            @endif


        </div>

    </div>
    <div class="footer-wrapper">
        <div class="footer-section f-section-1">
            <p class="">Copyright © 2022 <a target="_blank" href="https://designreset.com">ALDHI</a>, Test OXY.</p>
        </div>
        <div class="footer-section f-section-2">
            <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                    class="feather feather-heart">
                    <path
                        d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z">
                    </path>
                </svg></p>
        </div>
    </div>
</div>
<!--  END CONTENT AREA  -->

@endsection


@section('js')



@endsection
