@extends('template.template')

{{-- SUBMENU --}}
@section('sub-menu')

<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item active" aria-current="page"><span>Pinjam</span></li>
</ol>

@endsection

@section('pinjam')

data-active="true"

@endsection

{{-- CSS --}}
@section('css')

<style>
    .table>tbody>tr>td {
        color: #000000;
    }
</style>

@endsection

@section('content')

<!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing">

            <div class="col-xl-12 col-lg-12 col-sm-12 mb-3">

                {{-- menampilkan error validasi --}}
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if ($message = Session::get('hijau'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif

                @if ($message = Session::get('merah'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif


                <button type="button" class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#tambah">
                    Pinjam Buku
                </button>


                <div class="card shadow mb-3">
                    <div class="card-body">
                        <div class="table-responsive mb-4 mt-4">
                            <table id="datatables" class="table nowrap table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Buku</th>
                                        <th>Siswa</th>
                                        <th>status</th>
                                        <th>Tanggal pinjam</th>
                                        <th>Tanggal kembali</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                @php
                                $no = 1;
                                @endphp
                                <tbody>
                                    @foreach ($pinjam as $data)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $data->getbuku['kode_buku'] }} - {{ $data->getbuku['judul'] }}</td>
                                        <td>{{ $data->getuser['name'] }}</td>
                                        <td>{{ $data['status'] }}</td>
                                        <td>{{ tanggal($data['tgl_pinjam']) }}</td>
                                        <td>
                                            @if (!empty($data['tgl_kembali']))
                                                {{ tanggal($data['tgl_kembali']) }}
                                            @else
                                                -
                                            @endif
                                            </td>
                                        <td class="text-center">
                                            @if (empty($data['tgl_kembali']))
                                            <button type="button" class="btn btn-dark btn-sm" data-toggle="modal"
                                                data-target="#edit{{ $data['id'] }}">
                                                Kembalikan
                                            </button>
                                            @else
                                            <button type="button" class="btn btn-dark btn-sm" disabled>
                                                Kembalikan
                                            </button>
                                            @endif

                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                                data-target="#hapus{{ $data['id'] }}">
                                                Hapus
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <div class="footer-wrapper">
        <div class="footer-section f-section-1">
            <p class="">Copyright © 2021 <a target="_blank" href="https://designreset.com">ALDHI</a>, Test OXY.</p>
        </div>
        <div class="footer-section f-section-2">
            <p class="">Coded with <a href="http://instagram.com/aldhi009" target="_blank" rel="noopener noreferrer"
                    class="text-primary">Aldhi</a></p>
        </div>
    </div>
</div>
<!--  END CONTENT AREA  -->


{{-- MODAL PINJAM --}}
<div class="modal fade" id="tambah">
    <div class="modal-dialog modal-lg">
        <form action="{{ route('pinjam.store') }}" method="post">
            @csrf
            @method('POST')
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Pinjam buku</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        x
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="id_buku">Buku</label>
                        <select name="id_buku" class="form-control" required>
                            <option value="">Pilih</option>
                            @foreach ($buku as $d)
                            <option value="{{ $d['id'] }}">{{ $d['kode_buku'] }} - {{ $d['judul'] }}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="id_user">Siswa</label>
                        <select name="id_user" class="form-control" required>
                            <option value="">Pilih</option>
                            @foreach ($user as $d)
                            <option value="{{ $d['id'] }}">{{ $d['name'] }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Batal</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </div>
        </form>
    </div>

</div>


{{-- EDIT BUKU  --}}
@foreach($pinjam as $datapinjam)

<div class="modal fade" id="edit{{ $datapinjam['id'] }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="{{ route('pinjam.update', $datapinjam['id']) }}" method="post">
            @csrf
            @method('PUT')
            <div class="modal-content">


                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Mengembalikan buku {{ $datapinjam->getbuku['judul'] }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        x
                    </button>
                </div>
                <div class="modal-body">
                    Apakah buku sudah sesuai?
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Tidak</button>
                    <button type="submit" class="btn btn-warning">Iya</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endforeach

{{-- MODAL HAPUS KARYAWAN --}}
@foreach($pinjam as $datapinjam)
<div class="modal fade" id="hapus{{ $datapinjam['id'] }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h5 class="modal-title">Hapus  {{ $datapinjam->getuser['name'] }}<br> pinjam {{ $datapinjam->getbuku['judul'] }}</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <h6>Data yang dihapus tidak akan bisa dikembalikan.</h6>
            </div>
            <form action="{{ route('pinjam.destroy', $datapinjam['id']) }}" method="post">
                @csrf
                @method('DELETE')
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Batal</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach


@endsection


@section('js')

<script>
    $('#datatables').DataTable({
        dom: '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-10"i><"col-md-2 mb-3"p>>> >',
        buttons: {
            buttons: [{
                    extend: 'copy',
                    className: 'btn',
                    footer: true
                },
                {
                    extend: 'csv',
                    className: 'btn',
                    footer: true
                },
                {
                    extend: 'excel',
                    className: 'btn',
                    footer: true
                },
                {
                    extend: 'print',
                    className: 'btn',
                    footer: true
                }
            ]
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Results :  _MENU_",
        },
        "stripeClasses": [],
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        "pageLength": 10,
        "pagingType": "simple",

    });
</script>

@endsection
